import os
import atexit
from apscheduler.schedulers.background import BackgroundScheduler

from app import create_app

app = create_app(os.getenv("FLASK_ENV") or "dev")


def refresh_stocks():
    from app.stocks.fetcher import Fetcher

    with app.app_context():
        Fetcher().refresh()


if not app.debug or os.environ.get("WERKZEUG_RUN_MAIN") == "true":
    cron = BackgroundScheduler()
    cron.add_job(func=refresh_stocks, trigger="interval", minutes=60)
    cron.start()

    atexit.register(lambda: cron.shutdown())

if __name__ == "__main__":
    app.run(debug=True)
