Lima Stocks
=============
--------------------------------------------------
![Alt text](/readme/lima-stocks-server-logo.png)
--------------------------------------------------

Description
--------------

This is a small service that scraps stocks prices from the Lima Stocks Exchange website. It has calls to return all the active stocks, a specific stock and also price history.

I have used [AJ Pryor blog post](http://alanpryorjr.com/2019-05-20-flask-api-example/) as a template. I wrote it to **learn/explore** Python and Flask.

-------------

![Alt text](/readme/lima-stocks-server.gif)

-------------
