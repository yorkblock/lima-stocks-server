import os
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from app import create_app, db


env = os.getenv("FLASK_ENV") or "dev"
print(f"Active environment: * {env} *")
app = create_app(env)

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command("db", MigrateCommand)

app.app_context().push()


@manager.command
def run():
    app.run()


@manager.command
def init_db():
    print("Creating all resources.")
    db.create_all()


@manager.command
def drop_all():
    print("Dropping tables...")
    db.drop_all()


if __name__ == "__main__":
    manager.run()
