def register_routes(api, app, root="api"):
    from app.stocks import register_routes as attach_stocks
    from app.days import register_routes as attach_days

    attach_stocks(api, app)
    attach_days(api, app)
