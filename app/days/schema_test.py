from pytest import fixture
from datetime import date

from .model import Day
from .schema import DaySchema
from .interface import DayInterface


@fixture
def schema() -> DaySchema:
    return DaySchema()


def test_DaySchema_create(schema: DaySchema):
    assert schema


def test_DaySchema_works(schema: DaySchema):
    params: DayInterface = schema.load(
        {"dayId": 1, "date": "2020-05-31", "value": "2.05"}
    )
    day = Day(**params)

    assert day.day_id == 1
    assert day.date == date(2020, 5, 31)
    assert day.value == 2.05
