from datetime import date
from flask.testing import FlaskClient

from app.test.fixtures import client, app  # noqa
from .service import DayService
from .schema import DaySchema
from .interface import DayInterface  # noqa
from .model import Day
from . import BASE_ROUTE
from . import VERSION


def make_day(
    day_id: int = 1, date: date = date(2020, 6, 1), value: float = 2.05
) -> Day:
    return Day(day_id=day_id, date=date, value=value)


class TestDayResource:
    def test_get(self, monkeypatch, client: FlaskClient):  # noqa
        days = [make_day(), make_day(day_id=2, date=date(2020, 6, 2), value=2.06)]
        monkeypatch.setattr(DayService, "get", lambda s, d: days)

        with client:
            result = client.get(
                f"/api/{VERSION}/{BASE_ROUTE}/2/CASAGRC1", follow_redirects=True
            ).get_json()
            expected = DaySchema(many=True).dump(days)

            assert result == expected
