from datetime import date
from mypy_extensions import TypedDict


class DayInterface(TypedDict, total=False):
    day_id: int
    date: date
    value: float
