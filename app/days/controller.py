from typing import List
from flask_accepts import accepts, responds  # noqa
from flask_restx import Namespace, Resource

from app import auth
from .schema import DaySchema
from .service import DayService
from .model import Day

api = Namespace("Days", description="Historic Stocks Values")


@api.route("/<int:days>/<symbol>")
class DayResource(Resource):
    """Days"""

    @auth.login_required
    @responds(schema=DaySchema(many=True))
    def get(self, days: int, symbol: str) -> List[Day]:
        """Get historic values"""

        return DayService.get(symbol, days)
