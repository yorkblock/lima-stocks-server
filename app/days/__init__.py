from .model import Day  # noqa
from .schema import DaySchema  # noqa

BASE_ROUTE = "days"
VERSION = "v1"


def register_routes(api, app, root="api"):
    from .controller import api as day_api

    api.add_namespace(day_api, path=f"/{root}/{VERSION}/{BASE_ROUTE}")
