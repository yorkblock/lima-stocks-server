from marshmallow import fields, Schema


class DaySchema(Schema):
    """Day schema"""

    dayId = fields.Integer(attribute="day_id")
    date = fields.Date(attribute="date")
    value = fields.Number(attribute="value")
