from pytest import fixture
from pathlib import Path
from .fetcher import Fetcher


@fixture
def data():
    dir = Path(__file__).parent
    data = dir / "days.html"
    with open(data, "r", encoding="utf-8", errors="ignore") as file:
        return file.read()


def test_Fetcher_works(data, requests_mock):
    fetcher = Fetcher()
    fetcher.prepare(20, "EXSAI1")
    requests_mock.get(fetcher.url, text=data)

    days = Fetcher().fetch_days(20, "EXSAI1")
    assert len(days) == 20
    assert days[4].value == 1.75
