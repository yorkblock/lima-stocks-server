from datetime import date


class Day:
    def __init__(self, day_id: int, date: date, value: float):
        self.day_id = day_id
        self.date = date
        self.value = value
