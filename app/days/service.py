from typing import List
from .model import Day
from .fetcher import Fetcher


class DayService:
    @staticmethod
    def get(symbol: str, days: int) -> List[Day]:
        return Fetcher().fetch_days(days, symbol)
