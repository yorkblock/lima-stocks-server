from pytest import fixture
from datetime import date
from .model import Day
from .interface import DayInterface


@fixture
def interface() -> DayInterface:
    return DayInterface(day_id=1, date=date(2020, 6, 1), value=2.50)


def test_DayInterface_create(interface: DayInterface):
    assert interface


def test_DayInterface_work(interface: DayInterface):
    day = Day(**interface)
    assert day
