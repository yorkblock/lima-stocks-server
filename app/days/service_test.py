from typing import List
from .model import Day
from .service import DayService
from .fetcher import Fetcher

# from app.test.fixtures import app


def make_day(day_id=1, date="20200601", value=1.24):
    return Day(day_id=day_id, date=date, value=value)


def test_gat_two_days(monkeypatch):
    monkeypatch.setattr(
        Fetcher,
        "fetch_days",
        lambda obj, days, symbol: [make_day(), make_day(day_id=2)],
    )

    results: List[Day] = DayService.get(symbol="CASAGRC1", days=2)
    assert len(results) == 2
