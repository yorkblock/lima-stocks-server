from pytest import fixture
from datetime import date
from .model import Day


@fixture
def day() -> Day:
    return Day(day_id=1, date=date(2020, 6, 1), value=2.00)


def test_Day_create(day: Day):
    assert day
