import locale
import requests
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
from .model import Day
from .interface import DayInterface


class Fetcher:
    URL_LSX = "https://documents.bvl.com.pe/jsp/cotizacion.jsp?fec_inicio=$S&fec_fin=$E&nemonico=$N"

    def __init__(self, url=URL_LSX):
        self.url = url
        locale.setlocale(locale.LC_ALL, "en_US.UTF-8")

    def fetch_days(self, days, symbol):
        """
        Returns a dictionary array with dates and
        closed values for a stock in the last days.
        """
        self.prepare(days, symbol)
        rsp = self.request()

        return self.parse(rsp)

    def prepare(self, days, symbol):
        delta = timedelta(days=days)
        start = datetime.today() - delta

        url = self.url
        url = url.replace("$S", start.strftime("%Y%m%d"))
        url = url.replace("$E", datetime.today().strftime("%Y%m%d"))
        self.url = url.replace("$N", symbol)

    def request(self):
        print(f"url: {self.url}")
        return requests.get(
            self.url, verify=False, headers={"User-Agent": "Mozilla/5.0"}
        )

    def parse(self, rsp):
        days = []
        soup = BeautifulSoup(rsp.text, "html.parser")
        i, day_id = 0, 0
        trs = soup.find_all("tr")
        for tr in trs:
            if i < 2:
                i = i + 1
                continue

            stri = tr.contents[1].string
            date = datetime.strptime(stri, "%d/%m/%Y")

            valu = trim(tr.contents[19].string, chrs=[" ", ","])
            if valu is None:
                continue

            day_id = day_id + 1

            dayInterface = DayInterface(day_id=day_id, date=date, value=float(valu))
            days.append(Day(**dayInterface))

        return days[::-1]


def trim(str, chrs=[]):
    """ Removes any given characters plus \xa0. """
    if str is None:
        return None

    try:
        str = str.replace("\xa0", "")
    except TypeError:
        return None

    for chr in chrs:
        try:
            str = str.replace(chr, "")
        except TypeError:
            return None

    if str == "":
        return None

    return str
