from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_restx import Api
from flask_httpauth import HTTPTokenAuth

db = SQLAlchemy()

auth = HTTPTokenAuth(header="X-API-Key")
api_token = None


@auth.verify_token
def verify_token(token):
    return token is not None and token == api_token


def create_app(env=None):
    from app.config import config_by_name
    from app.routes import register_routes

    app = Flask(__name__)
    app.config.from_object(config_by_name[env or "dev"])

    authorizations = {
        "API Key": {"type": "apiKey", "in": "header", "name": "X-API-Key"}
    }

    global api_token
    api_token = app.config["APIKEY"]

    api = Api(
        app,
        title="Stocks API",
        version="1.0",
        security="API Key",
        authorizations=authorizations,
    )

    register_routes(api, app)
    db.init_app(app)

    @app.route("/api/v1/ping")
    def ping():
        return jsonify("version 1.0")

    return app
