from app.test.fixtures import app, client  # noqa


def test_app_creates(app):  # noqa
    assert app


def test_app_ping(app, client):  # noqa
    with client:
        resp = client.get("/api/v1/ping")
        assert resp.status_code == 200
        assert resp.is_json
        assert resp.json == "version 1.0"
