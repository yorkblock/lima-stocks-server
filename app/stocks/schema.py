from marshmallow import fields, Schema


class StockSchema(Schema):
    """Stock schema"""

    stockId = fields.Integer(attribute="stock_id")
    symbol = fields.String(attribute="symbol")
    name = fields.String(attribute="name")
    sector = fields.String(attribute="sector")
    currentValue = fields.String(attribute="current_value")
    lastDate = fields.Date(attribute="last_date")
    openingValue = fields.String(attribute="opening_value")
    lastValue = fields.String(attribute="last_value")
    sellOffer = fields.String(attribute="sell_offer")
    buyOffer = fields.String(attribute="buy_offer")
    stocksTraded = fields.String(attribute="stocks_traded")
    operations = fields.String(attribute="operations")
    variation = fields.String(attribute="variation")
    currency = fields.String(attribute="currency")
    amountTraded = fields.String(attribute="amount_traded")
    summaryLink = fields.String(attribute="summary_link")
    created = fields.DateTime(attribute="created")
