from sqlalchemy import Integer, Column, String, Date, DateTime, func
from datetime import datetime
from app import db


class Stock(db.Model):
    """Single stock"""

    __tablename__ = "stock"

    stock_id = Column(Integer(), primary_key=True)
    symbol = Column(String(10), index=True, nullable=False, unique=True)
    name = Column(String(100))
    sector = Column(String(50))
    current_value = Column(String(10))
    last_date = Column(Date())
    opening_value = Column(String(10))
    last_value = Column(String(10))
    sell_offer = Column(String(10))
    buy_offer = Column(String(10))
    stocks_traded = Column(String(10))
    operations = Column(String(10))
    variation = Column(String(10))
    currency = Column(String(3))
    amount_traded = Column(String(10))
    summary_link = Column(String(250))
    created = Column(DateTime(timezone=True), server_default=func.now())
