from mypy_extensions import TypedDict
from datetime import date, datetime


class StockInterface(TypedDict, total=False):
    stock_id: int
    symbol: str
    name: str
    sector: str
    current_value: str
    last_date: date
    opening_value: str
    last_value: str
    sell_offer: str
    stocks_traded: str
    operations: str
    variation: str
    currency: str
    amount_traded: str
    summary_link: str
    created: datetime
