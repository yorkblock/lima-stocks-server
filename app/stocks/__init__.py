from .model import Stock  # noqa
from .schema import StockSchema  # noqa

BASE_ROUTE = "stocks"
VERSION = "v1"


def register_routes(api, app, root="api"):
    from .controller import api as stock_api

    api.add_namespace(stock_api, path=f"/{root}/{VERSION}/{BASE_ROUTE}")
