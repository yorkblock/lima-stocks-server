from flask import request
from flask_accepts import accepts, responds  # noqa
from flask_restx import Namespace, Resource, abort
from typing import List

from app import auth
from .schema import StockSchema
from .service import StockService
from .model import Stock


api = Namespace("Stocks", description="Lima Stocks Exchange")


@api.route("/")
class StockResource(Resource):
    """Stocks"""

    @auth.login_required
    @accepts(dict(name="search", type=str), dict(name="symbols", type=str), api=api)
    @responds(schema=StockSchema(many=True), api=api)
    def get(self) -> List[Stock]:
        """Get all Stocks"""

        pattern: str = (request.args.get("search") or "").strip()
        if pattern:
            return StockService.search(pattern)

        symbols: [str] = (request.args.get("symbols") or "").split(",")
        if symbols[0]:
            return StockService.get_many(symbols)

        return StockService.get_all()


@api.route("/<symbol>")
@api.response(404, "Symbol not found")
class StockSymbolResource(Resource):
    """Single Stock"""

    @auth.login_required
    @responds(schema=StockSchema(), api=api)
    def get(self, symbol: str) -> Stock:
        """Get single stock"""

        stock = StockService.get(symbol)
        if stock is None:
            abort(404)

        return stock
