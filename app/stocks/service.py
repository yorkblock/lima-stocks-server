from typing import List
from .model import Stock
from .fetcher import Fetcher


class StockService:
    @staticmethod
    def refresh():
        Fetcher().refresh()

    @staticmethod
    def get_all() -> List[Stock]:
        return Stock.query.all()

    @staticmethod
    def get(symbol: str) -> Stock:
        return Stock.query.filter(Stock.symbol == symbol).first()

    @staticmethod
    def get_many(symbols: [str]) -> List[Stock]:
        return Stock.query.filter(Stock.symbol.in_(symbols)).all()

    @staticmethod
    def search(match: str) -> List[Stock]:
        m = f"%{match}%"
        return Stock.query.filter(
            ((Stock.symbol.ilike(m)) | (Stock.name.ilike(m)) | (Stock.sector.ilike(m)))
        ).all()
