import requests
import urllib3
from datetime import datetime
from bs4 import BeautifulSoup
from .interface import StockInterface
from .model import Stock
from app import db

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class Fetcher:
    URL_LSX = "https://documents.bvl.com.pe/includes/cotizaciones_todas.dat"

    def __init__(self, url=URL_LSX):
        self.url = url

    def refresh(self):
        print(f"Start Refreshing...{datetime.now()}")
        stocks = self.fetch_stocks()
        if stocks is None or len(stocks) == 0:
            print(f"End Refreshing...WARN: no stock fetched {datetime.now()}")
            return

        Stock.query.delete()
        db.session.commit()
        for stock in stocks:
            db.session.add(stock)
        db.session.commit()
        db.session.close()
        print(f"End Refreshing...{datetime.now()}")

    def fetch_stocks(self):
        rsp = requests.get(
            self.url, verify=False, headers={"User-Agent": "Mozilla/5.0"}
        )
        return self.parse(rsp)

    def parse(self, rsp):
        stocks = []

        soup = BeautifulSoup(rsp.text, "html.parser")
        i, stid = 0, 0
        for dtls in soup.find_all("tr"):
            if i < 2:
                i = i + 1
                continue

            stid += 1
            name = trim(dtls.contents[1].string)
            symb = trim(dtls.contents[2].string)
            sect = trim(dtls.contents[3].string)
            curr = trim(dtls.contents[5].string)
            if curr == "US$":
                curr = "USD"
            elif curr == "S/":
                curr = "PEN"
            else:
                curr = None
            lval = trim(dtls.contents[6].string, chrs=[" ", ","])
            ldat = trim(dtls.contents[7].string)
            if ldat is not None:
                ldat = datetime.strptime(ldat, "%d/%m/%Y")
            oval = trim(dtls.contents[8].string, chrs=[" ", ","])
            cval = trim(dtls.contents[9].string, chrs=[" ", ","])
            vari = trim(dtls.contents[10].string, chrs=[" ", ","])
            boff = trim(dtls.contents[11].string, chrs=[" ", ","])
            soff = trim(dtls.contents[12].string, chrs=[" ", ","])
            stra = trim(dtls.contents[13].string, chrs=[" ", ","])
            oper = trim(dtls.contents[14].string, chrs=[" ", ","])
            atra = trim(dtls.contents[15].string, chrs=[" ", ","])
            try:
                slin = trim(Fetcher.URL_LSX + dtls.a["href"])
            except TypeError:
                slin = None

            stockInterface = StockInterface(
                stock_id=stid,
                symbol=symb,
                name=name,
                sector=sect,
                current_value=cval,
                last_date=ldat,
                opening_value=oval,
                last_value=lval,
                sell_offer=soff,
                buy_offer=boff,
                stocks_traded=stra,
                operations=oper,
                variation=vari,
                currency=curr,
                amount_traded=atra,
                summary_link=slin,
            )

            stocks.append(Stock(**stockInterface))

        return stocks


def trim(str, chrs=[]):
    """ Removes any given characters plus \xa0. """
    if str is None:
        return None

    try:
        str = str.replace("\xa0", "")
    except TypeError:
        return None

    for chr in chrs:
        try:
            str = str.replace(chr, "")
        except TypeError:
            return None

    if str == "":
        return None

    return str
