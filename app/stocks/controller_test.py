from datetime import date
from flask.testing import FlaskClient

from app.test.fixtures import client, app  # noqa
from .service import StockService
from .schema import StockSchema
from .model import Stock
from .interface import StockInterface  # noqa
from . import BASE_ROUTE
from . import VERSION


def make_stock(
    id: int = 1, symbol: str = "CASAGRC1", name: str = "Casa Grande"
) -> Stock:
    stock = Stock(
        stock_id=id,
        symbol=symbol,
        name=name,
        sector="Agraria",
        current_value="3.21",
        last_date=date.today(),
        opening_value="3.13",
        last_value="3.15",
        sell_offer="3.30",
        buy_offer="3.28",
        stocks_traded="123",
        operations="5",
        variation="1.2",
        currency="pen",
        amount_traded="5002.00",
        summary_link="https://bvl.com.pe",
    )

    return stock


class TestStockResource:
    def test_get_all(self, monkeypatch, client: FlaskClient):  # noqa
        stocks = [make_stock(), make_stock(2, symbol="APPL", name="Apple")]
        monkeypatch.setattr(
            StockService, "get_all", lambda: stocks,
        )

        with client:
            results = client.get(
                f"/api/{VERSION}/{BASE_ROUTE}", follow_redirects=True
            ).get_json()
            s = [make_stock(), make_stock(2, symbol="APPL", name="Apple")]
            expected = StockSchema(many=True).dump(s)
            for r in results:
                assert r in expected


class TestStockSymbolResource:
    def test_get(self, monkeypatch, client: FlaskClient):  # noqa
        stock = make_stock()
        monkeypatch.setattr(StockService, "get", lambda s: stock)

        with client:
            result = client.get(
                f"/api/{VERSION}/{BASE_ROUTE}/CASAGRC1", follow_redirects=True
            ).get_json()
            expected = StockSchema().dump(make_stock())

            assert result == expected
