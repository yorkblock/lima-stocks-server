from pytest import fixture
from flask_sqlalchemy import SQLAlchemy
from datetime import date
from app.test.fixtures import app, db  # noqa
from .model import Stock


@fixture
def stock() -> Stock:
    return Stock(
        stock_id=1,
        symbol="CASAGRC1",
        name="Casa Grande",
        sector="Agraria",
        current_value="3.21",
        last_date=date.today(),
        opening_value="3.13",
        last_value="3.15",
        sell_offer="3.30",
        buy_offer="3.28",
        stocks_traded="123",
        operations="5",
        variation="1.2",
        currency="pen",
        amount_traded="5002.00",
        summary_link="https://bvl.com.pe",
    )


def test_Stock_create(stock: Stock):
    assert stock


def test_Stock_retrieve(stock: Stock, db: SQLAlchemy):  # noqa
    db.session.add(stock)
    db.session.commit()
    s = Stock.query.first()
    assert s.__dict__ == stock.__dict__
