from pytest import fixture
from datetime import date, datetime

from .model import Stock
from .schema import StockSchema
from .interface import StockInterface


@fixture
def schema() -> StockSchema:
    return StockSchema()


def test_StockSchema_create(schema: StockSchema):
    assert schema


def test_StockSchema_works(schema: StockSchema):
    params: StockInterface = schema.load(
        {
            "stockId": "1",
            "symbol": "CASAGRC1",
            "name": "Casa Grande",
            "sector": "Agraria",
            "currentValue": "3.21",
            "lastDate": "2020-04-30",
            "openingValue": "3.13",
            "lastValue": "3.15",
            "sellOffer": "3.30",
            "buyOffer": "3.28",
            "stocksTraded": "123",
            "operations": "5",
            "variation": "1.2",
            "currency": "pen",
            "amountTraded": "5002.00",
            "summaryLink": "https://bvl.com.pe",
            "created": "2020-04-30 19:42:52.384267",
        }
    )
    stock = Stock(**params)

    assert stock.stock_id == 1
    assert stock.symbol == "CASAGRC1"
    assert stock.name == "Casa Grande"
    assert stock.sector == "Agraria"
    assert stock.current_value == "3.21"
    assert stock.last_date == date(2020, 4, 30)
    assert stock.opening_value == "3.13"
    assert stock.last_value == "3.15"
    assert stock.sell_offer == "3.30"
    assert stock.buy_offer == "3.28"
    assert stock.stocks_traded == "123"
    assert stock.operations == "5"
    assert stock.variation == "1.2"
    assert stock.currency == "pen"
    assert stock.amount_traded == "5002.00"
    assert stock.summary_link == "https://bvl.com.pe"
    assert stock.created == datetime(2020, 4, 30, 19, 42, 52, 384267)
