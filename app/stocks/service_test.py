from datetime import date
from typing import List
from flask_sqlalchemy import SQLAlchemy

from .model import Stock
from .service import StockService
from app.test.fixtures import app, db  # noqa


def make_stock(id: int = 1, symbol: str = "CASAGRC1"):
    stock = Stock(
        stock_id=id,
        symbol=symbol,
        name="Casa Grande",
        sector="Agraria",
        current_value="3.21",
        last_date=date.today(),
        opening_value="3.13",
        last_value="3.15",
        sell_offer="3.30",
        buy_offer="3.28",
        stocks_traded="123",
        operations="5",
        variation="1.2",
        currency="pen",
        amount_traded="5002.00",
        summary_link="https://bvl.com.pe",
    )

    return stock


def test_get_all(monkeypatch, db: SQLAlchemy):  # noqa
    db.session.add(make_stock())
    db.session.commit()
    results: List[Stock] = StockService.get_all()

    assert len(results) == 1
    assert results[0].sector == "Agraria"


def test_get(monkeypatch, db: SQLAlchemy):  # noqa
    db.session.add(make_stock())
    db.session.commit()
    stock: Stock = StockService.get("CASAGRC1")

    assert type(stock) is Stock
    assert stock.name == "Casa Grande"


def test_get_many(monkeypatch, db: SQLAlchemy):  # noqa
    db.session.add(make_stock())
    db.session.add(make_stock(2, "APPL"))
    db.session.commit()

    results: List[Stock] = StockService.get_many(["CASAGRC1", "APPL"])

    assert len(results) == 2


def test_search(monkeypatch, db: SQLAlchemy):  # noqa
    db.session.add(make_stock())
    db.session.add(make_stock(2, "APPL"))
    db.session.commit()

    results: List[Stock] = StockService.search("appl")

    assert len(results) == 1
