from pytest import fixture
from pathlib import Path
from .fetcher import Fetcher


@fixture
def data():
    dir = Path(__file__).parent
    data = dir / "stocks.html"
    with open(data, "r") as file:
        return file.read()


def test_Fetcher_works(data, requests_mock):
    requests_mock.get(Fetcher.URL_LSX, text=data)
    stocks = Fetcher().fetch_stocks()
    assert len(stocks) == 369
