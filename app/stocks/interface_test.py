from pytest import fixture
from datetime import date, datetime
from .model import Stock
from .interface import StockInterface


@fixture
def interface() -> StockInterface:
    return StockInterface(
        stock_id=1,
        symbol="CSGRCASAGRC1",
        name="Casa Grande",
        sector="Agraria",
        current_value="3.21",
        last_date=date.today(),
        opening_value="3.13",
        last_value="3.15",
        sell_offer="3.30",
        buy_offer="3.28",
        stocks_traded="123",
        operations="5",
        variation="1.2",
        currency="pen",
        amount_traded="5002.00",
        summary_link="https://bvl.com.pe",
        created=datetime.now(),
    )


def test_StockInterface_create(interface: StockInterface):
    assert interface


def test_StockInterface_works(interface: StockInterface):
    stock = Stock(**interface)
    assert stock
