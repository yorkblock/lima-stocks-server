import os
from typing import List, Type


basedir = os.path.abspath(os.path.dirname(__file__))


class BaseConfig:
    CONFIG_NAME = "base"
    USE_MOCK_EQUIVALENCY = False
    DEBUG = False
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    APIKEY = None


class DevelopmentConfig(BaseConfig):
    CONFIG_NAME = "dev"
    SECRET_KEY = os.getenv("DEV_SECRET_KEY", "Development Secret")
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "postgresql:///lima_stocks_dev"
    APIKEY = os.getenv("LSX_API_KEY")


class TestingConfig(BaseConfig):
    CONFIG_NAME = "test"
    SECRET_KEY = os.getenv("TEST_SECRET_KEY", "Testing Secret")
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "postgresql:///lima_stocks_test"
    APIKEY = os.getenv("LSX_API_KEY")


class ProductionConfig(BaseConfig):
    CONFIG_NAME = "prod"
    SECRET_KEY = os.getenv("PROD_SECRET_KEY", "Prodcution Secret")
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = "postgresql:///lima_stocks_prod"
    APIKEY = os.getenv("LSX_API_KEY")


EXPORT_CONFIGS: List[Type[BaseConfig]] = [
    DevelopmentConfig,
    TestingConfig,
    ProductionConfig,
]
config_by_name = {cfg.CONFIG_NAME: cfg for cfg in EXPORT_CONFIGS}
